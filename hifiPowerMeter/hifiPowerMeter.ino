/*
 *hifi Power Meter: The Arduino Code needed for the hifi Power Meter Project
 * Allows logging at 500+ Hz by directly reading the square wave signal from an SRM, as well as data from a reedswitch
 * 
 * Find the documentation at: https://confluence.specialized.com/x/h2Q0Dg
 * I.Jungnickel 8/20/2018
 * 
 *  === File layout ===
 *  
 *  The file written to the SD card  (LOGXXXXX.TXT) has the following format
 *  - One number per row
 *  - If the number is positive, this number is the delta time since the last event where the coil was triggered in microseconds (-> Inverse of Frequency*1000000)
 *  - If the number is negative, this number is the delta time since the last event where the reed switch was triggered in microseconds (-> Inverse of Cadence*-60000)
 *  
 *  To get from the frequency of the coil to the Torque you have to subtract the zero frequency (The frequnecy the coil oscillates at when no load is applied, typically around 500), and apply the slope (19.0Hz per Nm. But you can also calibrate this yourself).
 */


//------------------------------------------------------------------------------
// Variable Definitions 
long oldTime; // Signal Timers
long newTime;
long dTime;
long oldTime2; //Reed Switch timers
long newTime2;
long dTime2;
int counter = 0; //Counter needed to batch up data for write.
String dataString = ""; // String used to batch up the data
String dataString2 = ""; // This is the string that actually gets send
boolean logFlag = false;


//------------------------------------------------------------------------------
void setup() {

  oldTime = micros();
  oldTime2 = millis();
  pinMode(LED_BUILTIN, OUTPUT); 
  digitalWrite(LED_BUILTIN, HIGH);    // turn the LED off by making the voltage LOW
  pinMode(11, INPUT_PULLUP); // Needed for the reed switch

  attachInterrupt(digitalPinToInterrupt(12), pwrSignal, RISING);
  attachInterrupt(digitalPinToInterrupt(11), cadenceSignal, RISING);
  
  Serial1.begin(115200); // The openlog is connected to the serial port. You can change all Serial1 to Serial, to output all data to the Serial port for debugging. 
}

//------------------------------------------------------------------------------
void loop() {
  if (logFlag == true) { // If the attach interrupt has triggered a logging operation data gets written to the card.
    dataString2 = dataString; // Copy over and clear the string, so that the string that gets written to the card is not the same that the interrupts write to (needed for performance reasons).
    dataString = "";
    counter = 0; // Reset the counter
    Serial1.print(dataString2); // Actually write the data
    logFlag = false; // Reset the flag and clear that string as well.
    dataString2 = "";
    } 
  }

//------------------------------------------------------------------------------
// The interrupt routine that gets called, when the coil triggers.
void pwrSignal() {

  newTime = micros(); // Measure the timing
  dTime = newTime - oldTime;
  oldTime = newTime;
  
  dataString += String(dTime); // Attach the data to the string
  dataString += "\r\n"; // Carriage return
  counter++;

  if (counter>10) {logFlag = true;} // Only trigger write to SD card if 10 samples have been logged. Needed for performance reasons. Might need to increase or decrease this number if logging is funky

}


//------------------------------------------------------------------------------
// The interrupt routine that gets called, when the reed switch triggers. Happens when the crank is at the 3 O Clock position
void cadenceSignal() {
  if (millis()-oldTime2> 200){ // Needed to debounce the signal. Cadence faster than 300RPM can't be detected.
  newTime2 = millis();  // Measure the timing
  dTime2 = newTime2 - oldTime2;
  oldTime2 = newTime2;

  
    //dataString += "-100\r\n";
    dTime2 = dTime2*-1; 
    dataString += String(dTime2); // Attach the data to the string
    dataString += "\r\n";
 
  }
}
