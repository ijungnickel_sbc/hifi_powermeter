%% HIFI POWERMETER VIEWER
% The code to process and visualize the power meter data
%
% I.Jungnickel (Ingmar.Jungnickel@specialized.com) 8/9/2018


%% Script Settings

% Zero Offset
f_Zero = 520; % [Hz]
% Slope
slope = 19.0; % [Hz/Nm}

%% Begin Code

% Load the file
try
    rawData = csvread("Data09.csv");
catch errorMSG
    warning (errorMSG.message);
    error 'File could not be read. There are probably corrupted lines in there. You need to delete those manually'; 
end

% Plot error metrics
subplot(2,1,1)
histogram(rawData(rawData>0),100);
title 'Power Meter Data'
subplot(2,1,2)
histogram(rawData(rawData<0)*-1,100);
title 'Cadence Data'

% Create time vector
t = nan(size(rawData));
t(1) = 0;
for i=2:length(rawData)
    if rawData(i)<0 
        t(i) = t(i-1)
    else
        t(i) = t(i-1) + rawData(i)/1000;
    end
  
end